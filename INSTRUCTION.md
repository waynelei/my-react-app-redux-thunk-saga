### Installation and Run
```
npx create-react-app my-react-app-redux-thunk-saga

cd my-react-app-redux-thunk-saga

npm start
```


### Configure webpack, babel presets using ejection
```
git add -A
git commit -m "commit before eject"

npm run eject

Note: after run, will generate config folder

```
#### Add 2 lines into /config/webpack.config.dev and /config/webpack.config.prod
```
    modules: true,
    localIdentName: '[name]__[local]___[hash:base64:5]'
```
```
https://fonts.google.com/

select 'Open Sans'

Family Selected -> CUSTOMIZE -> (regular 400 and bold 700) -> EMBED

copy "<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
" into index.html

```


### Install redux
```
npm install --save redux

import { createStore, applyMiddleware, compose, combineReducers } from "redux";

```

#### Run Sample with command 
```
node redux-basics.js
{ counter: 0 }
[Subscription] { counter: 1 }
[Subscription] { counter: 11 }
Last line { counter: 11 }
```

### install react-redux to connect to state
```
npm install --save react-redux

import { Provider, connect } from "react-redux";

```


### Install thunk
```
npm install --save redux-thunk

import thunk from 'redux-thunk';
```


### Install redux-saga
```
npm install --save redux-saga
```


### Install react-router and react-router-dom
```
npm install --save react-router react-router-dom
```