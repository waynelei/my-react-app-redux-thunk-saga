import React from 'react';

import classes from './ComponentTest.css';

const componentTest = (props) => {
    return (
        <div className={classes.ComponentTest}>
            <p>Hello {props.children ? props.children : 'world'}!</p>
        </div>
    );
};

export default componentTest;


