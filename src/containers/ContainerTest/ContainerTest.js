import React, { Component } from 'react';

import classes from './ContainerTest.css';

class ContainerTest extends Component {
    state = {
        date: new Date(),
        btnState: "Stop"
    }

    componentDidMount() {
        this.startTimer();
    }

    startTimer() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    stopTimer() {
        clearInterval(this.timerID);
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    tick() {
        this.setState({
            ...this.state,
            date: new Date()
        });
    }

    toggleButton() {
        let btnState = "Start";
        if ( this.state.btnState === "Start") {
            btnState = "Stop";
            this.startTimer();
        } else {
            this.stopTimer();
        }
        this.setState({
            ...this.state,
            btnState: btnState
        })
    }

    render() {

        return (
            <div className={classes.ContainerTest}>
                <p>Test Container</p>
                {this.state.date.toLocaleTimeString()}
                <div>
                    <button
                        onClick={() => this.toggleButton()}>
                        {this.state.btnState} timer
                    </button>
                </div>
            </div>
        )
    }

};

export default ContainerTest;