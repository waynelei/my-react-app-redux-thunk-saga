import React, { Component } from 'react';
import {connect} from 'react-redux';

import * as actions from '../../store/actions/index';

import classes from './ThunkTest.css';

class ThunkTest extends Component {

    componentWillMount(){
        this.props.thunkTest();
        // this.props.onFetchUser();
    }

    renderUser = (user) => {
        return (
            <div className="card card-block" key={user.name}>
                <h4 className="card-title">{user.name}</h4>
            </div>
        )
    }

    render() {
        return (
            <div className={classes.ContainerTest}>
                <p>Thunk Test</p>
                {this.props.users.map(this.renderUser)}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        users: state.thunkTest
    }
}

// const mapDispatchToProps = dispatch => {
//     return {
//         onFetchUser: () => dispatch( actions.testAction1() )
//     };
// };

export default connect(mapStateToProps, actions)(ThunkTest);