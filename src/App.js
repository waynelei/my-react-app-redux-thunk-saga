import React, { Component } from 'react';

import ComponentTest from './components/ComponentTest/ComponentTest';
import ContainerTest from './containers/ContainerTest/ContainerTest';
import ActionTest from './containers/ActionTest/ActionTest';
import ThunkTest from './containers/ThunkTest/ThunkTest';
import SagaTest from './containers/SagaTest/SagaTest';

class App extends Component {
  render() {
    return (
      <div>
        <ComponentTest >Wayne</ComponentTest>
        <ComponentTest />
        <ContainerTest />
        <ActionTest />
        <ThunkTest />
        <SagaTest />
      </div>
    );
  }
}

export default App;
