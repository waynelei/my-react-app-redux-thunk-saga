import { takeEvery, all, takeLatest } from "redux-saga/effects";

import * as types from "../actions/types";
import {
    testSaga
} from "./sagaTest";


export function* watchSagaTest() {
  yield all([
    takeEvery(types.SAGA_INIT_TEST, testSaga)
  ]);
}