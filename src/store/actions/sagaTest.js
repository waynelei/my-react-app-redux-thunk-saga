import * as types from "./types";

export const sagaTest = () => {
    return {
        type: types.SAGA_TEST,
        payload: [
            { name: 'saga one' },
            { name: 'saga two' }
        ]
    };
};

export const sagaInitTest = () => {
    return {
        type: types.SAGA_INIT_TEST
    };
};