import * as types from "./types";

export const testAction1 = (payload) => {
  return {
    type: types.TEST_ACTION_1,
    payload: [
      { name: 'wayne' },
      { name: 'jaye' },
      { name: 'david' },
    ]
  };
};

export const testAction2 = (payload) => {
    return {
      type: types.TEST_ACTION_2,
      payload: payload
    };
  };