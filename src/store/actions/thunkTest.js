import * as types from "./types";

export const thunkTest = (payload) => {

    return dispatch => {
        setTimeout(() => {
            dispatch(setData(payload));
        }, 2000);
    }


};

export const setData = (payload) => {
    return {
        type: types.THUNK_TEST,
        payload: [
            { name: 'wayne' },
            { name: 'jaye' }
        ]
    };
}
