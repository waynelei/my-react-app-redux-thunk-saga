import { combineReducers } from 'redux';
import testReducer from './actionTest';
import thunkReducer from './thunkTest';
import sagaReducer from './sagaTest';

const rootReducer = combineReducers({
    // state: (state = {} ) => state
    test: testReducer,
    thunkTest: thunkReducer,
    sagaTest: sagaReducer
});

export default rootReducer;