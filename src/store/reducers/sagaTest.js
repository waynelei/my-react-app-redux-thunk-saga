import {
    SAGA_TEST
} from '../actions/types';

export default function (state = [], action) {
    switch (action.type) {
        case SAGA_TEST:
            return [...state, ...action.payload];
        default:
            return state;
    }
}