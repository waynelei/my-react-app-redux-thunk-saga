import {
    TEST_ACTION_1,
    TEST_ACTION_2
} from '../actions/types';

export default function (state = [], action) {
    switch (action.type) {
        case TEST_ACTION_1:
            return [...state, ...action.payload];
        case TEST_ACTION_2:
            return [...state, ...action.payload];
        default:
            return state;
    }
}